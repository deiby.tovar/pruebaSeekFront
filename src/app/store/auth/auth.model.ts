export interface AuthState {
  token: string | null;
  user?: {
    email: string;
    name: string;
  };
}
