import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';
import { AuthState } from './auth.model';
import { DataResponseLogin } from '@models/response/login.response';

const initialState: AuthState = {
  token: localStorage.getItem('token')!,
  user: localStorage.getItem('user')
    ? JSON.parse(localStorage.getItem('user')!)
    : null,
};

export const AuthStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods((store) => ({
    updatedAuth(auth: DataResponseLogin) {
      localStorage.setItem('token', auth.token);
      localStorage.setItem('user', JSON.stringify(auth.user));
      patchState(store, { ...auth });
    },
  }))
);
