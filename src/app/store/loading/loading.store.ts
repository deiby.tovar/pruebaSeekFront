import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';
import { LoadingState } from './loading.model';

const initialState: LoadingState = {
  isLoading: false,
};

export const loadingStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods(({ isLoading, ...store }) => ({
    changeLoadingState(isLoading: boolean) {
      patchState(store, { isLoading });
    },
  }))
);
