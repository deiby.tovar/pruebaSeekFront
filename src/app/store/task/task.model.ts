import { Task } from '@models/model/task.model';

export interface TaskState {
  tasks: Task[];
}
