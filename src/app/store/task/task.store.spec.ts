import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { TaskStore } from './task.store';
import { TodoService } from '@services/todo.service';
import { Task } from '@models/model/task.model';
import {
  TaskListResponse,
  TaskResponse,
} from '@models/response/tasks.response';
import { HttpClientModule } from '@angular/common/http';

describe('TaskStore', () => {
  let taskStore: any;
  let todoService: TodoService;

  const initialTasks: Task[] = [
    { id: 1, title: 'Task 1', description: 'asdasd', status: 'por hacer' },
    { id: 2, title: 'Task 2', description: 'asdasd', status: 'por hacer' },
  ];

  const mockTaskResponse: TaskResponse = {
    message: 'message',
    data: {
      id: 3,
      title: 'Task 3',
      description: 'asdasd',
      status: 'por hacer',
    },
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
    });

    todoService = TestBed.inject(TodoService);
    taskStore = TestBed.inject(TaskStore);
  });

  it('should load all tasks', async () => {
    const mockResponse: TaskListResponse = { message: '', data: initialTasks };
    spyOn(todoService, 'getAll').and.returnValue(of(mockResponse));
    await taskStore.loadAll();
    expect(taskStore.tasks()).toEqual(initialTasks);
  });

  it('should add a task', async () => {
    spyOn(todoService, 'addTaks').and.returnValue(of(mockTaskResponse));
    await taskStore.addTask(mockTaskResponse.data);
    expect(taskStore.tasks()).toContain(mockTaskResponse.data);
  });

  it('should update a task', async () => {
    const updatedTask: Task = {
      id: 1,
      title: 'Task 1',
      description: 'upadted',
      status: 'por hacer',
    };
    spyOn(todoService, 'addTaks').and.returnValue(
      of({
        message: '',
        data: {
          id: 1,
          title: 'Task 1',
          description: 'asdasd',
          status: 'por hacer',
        },
      })
    );
    await taskStore.addTask(mockTaskResponse.data);

    const mockResponse: TaskResponse = { message: '', data: updatedTask };

    spyOn(todoService, 'updateTask').and.returnValue(of(mockResponse));

    await taskStore.updateTask(1, updatedTask);
    const task = taskStore.tasks().find((t: any) => t.id === 1);
    expect(task).toEqual(updatedTask);
    expect(todoService.updateTask).toHaveBeenCalledWith(1, updatedTask);
  });

  it('should delete a task', async () => {
    spyOn(todoService, 'addTaks').and.returnValue(
      of({
        message: '',
        data: {
          id: 1,
          title: 'Task 1',
          description: 'asdasd',
          status: 'por hacer',
        },
      })
    );
    await taskStore.addTask(mockTaskResponse.data);
    spyOn(todoService, 'deleteTask').and.returnValue(of({ message: '' }));
    await taskStore.deleteTask(1);
    const task = taskStore.tasks().find((t: any) => t.id === 1);
    expect(task).toBeUndefined();
    expect(taskStore.tasks().length).toBe(0);
    expect(todoService.deleteTask).toHaveBeenCalledWith(1);
  });
});
