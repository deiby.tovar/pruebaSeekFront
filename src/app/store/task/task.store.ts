import { Task } from '@models/model/task.model';
import { TaskState } from './task.model';

import { inject } from '@angular/core';
import {
  TaskListResponse,
  TaskResponse,
} from '@models/response/tasks.response';
import { patchState, signalStore, withMethods, withState } from '@ngrx/signals';
import { TodoService } from '@services/todo.service';
import { lastValueFrom } from 'rxjs';

const initialState: TaskState = {
  tasks: [],
};

export const TaskStore = signalStore(
  { providedIn: 'root' },
  withState(initialState),
  withMethods(({ tasks, ...store }, todoService = inject(TodoService)) => ({
    async loadAll() {
      const response: TaskListResponse = await lastValueFrom(
        todoService.getAll()
      );
      patchState(store, { tasks: response.data });
    },
    async addTask(task: Task) {
      const response: TaskResponse = await lastValueFrom(
        todoService.addTaks(task)
      );
      const nuewTask = [...tasks(), response.data!];
      patchState(store, { tasks: nuewTask });
    },

    async updateTask(id: number, task: Task) {
      const response: TaskResponse = await lastValueFrom(
        todoService.updateTask(id, task)
      );
      const tasksState = tasks().map((item) => {
        if (item.id === id) {
          item = { ...response.data! };
        }
        return item;
      });
      patchState(store, { tasks: tasksState });
    },

    async deleteTask(id: number) {
      await lastValueFrom(todoService.deleteTask(id));
      const tasksState = tasks().filter((item) => item.id !== id);
      patchState(store, { tasks: tasksState });
    },
  }))
);
