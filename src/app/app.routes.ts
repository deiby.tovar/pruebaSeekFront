import { Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { authGuard } from './shared/guards/auth.guard';

export const routes: Routes = [
  { path: '', redirectTo: '/todo', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'todo',
    canActivate: [authGuard],
    loadComponent: () =>
      import('./pages/todo/todo.component').then((c) => c.TodoComponent),
  },

  { path: '**', redirectTo: '/todo' },
];
