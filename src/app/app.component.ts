import { CommonModule } from '@angular/common';
import { Component, inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SharedNgZorroModule } from '@modules/shared-ng-zorro/shared-ng-zorro.module';
import { loadingStore } from './store/loading/loading.store';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, SharedNgZorroModule, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  readonly store = inject(loadingStore);
  title = 'pruebaSeek';
}
