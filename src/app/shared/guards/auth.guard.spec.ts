import { TestBed } from '@angular/core/testing';
import { CanActivateFn } from '@angular/router';

import { authGuard } from './auth.guard';
import { AuthStore } from '../../store/auth/auth.store';

describe('authGuard', () => {
  const executeGuard: CanActivateFn = () =>
    TestBed.runInInjectionContext(() => authGuard({} as any, {} as any));

  beforeEach(() => {
    TestBed.configureTestingModule({});
  });

  it('should be created', () => {
    executeGuard({} as any, {} as any);
    expect(executeGuard).toBeTruthy();
  });
});
