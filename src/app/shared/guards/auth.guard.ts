import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { AuthStore } from 'app/store/auth/auth.store';

export const authGuard: CanActivateFn = () => {
  const authStore = inject(AuthStore);

  return authStore.token() ? true : inject(Router).createUrlTree(['/login']);
};
