import { NgModule } from '@angular/core';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzIconModule } from 'ng-zorro-antd/icon';

@NgModule({
  declarations: [],
  imports: [
    NzInputModule,
    NzButtonModule,
    NzFormModule,
    NzSpinModule,
    NzTableModule,
    NzModalModule,
    NzCardModule,
    NzSelectModule,
    NzIconModule,
  ],
  exports: [
    NzInputModule,
    NzButtonModule,
    NzFormModule,
    NzSpinModule,
    NzTableModule,
    NzModalModule,
    NzCardModule,
    NzSelectModule,
    NzIconModule,
  ],
})
export class SharedNgZorroModule {}
