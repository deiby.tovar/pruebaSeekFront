import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { LoginResponse } from '@models/response/login.response';
import { AuthStore } from 'app/store/auth/auth.store';
import { environment } from 'environments/environment';
import { Observable, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly http: HttpClient = inject(HttpClient);

  store = inject(AuthStore);

  baseUrl = environment.urlApi;

  login(data: any): Observable<LoginResponse> {
    return this.http.post<LoginResponse>(`${this.baseUrl}login`, data).pipe(
      tap((resp) => {
        this.store.updatedAuth(resp.data);
      })
    );
  }
}
