import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Task } from '@models/model/task.model';
import {
  TaskListResponse,
  TaskResponse,
} from '@models/response/tasks.response';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private readonly http: HttpClient = inject(HttpClient);

  baseUrl = environment.urlApi;

  getAll(): Observable<TaskListResponse> {
    return this.http.get<TaskListResponse>(`${this.baseUrl}tasks`);
  }

  addTaks(task: Task): Observable<TaskResponse> {
    return this.http.post<TaskResponse>(`${this.baseUrl}tasks`, task);
  }

  updateTask(id: number, task: Task): Observable<TaskResponse> {
    return this.http.put<TaskResponse>(`${this.baseUrl}tasks/${id}`, task);
  }

  deleteTask(id: number): Observable<TaskResponse> {
    return this.http.delete<TaskResponse>(`${this.baseUrl}tasks/${id}`);
  }
}
