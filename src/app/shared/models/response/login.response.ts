export interface LoginResponse {
  message: string;
  data: DataResponseLogin;
}

export interface DataResponseLogin {
  token: string;
  user: {
    email: string;
    name: string;
  };
}
