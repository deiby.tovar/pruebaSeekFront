import { Task } from '@models/model/task.model';

export interface TaskResponse {
  message: string;
  data?: Task;
}

export interface TaskListResponse {
  message: string;
  data: Task[];
}
