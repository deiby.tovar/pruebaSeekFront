import { FormControl } from '@angular/forms';

export interface TaskForm {
  title: FormControl<string | null>;
  description: FormControl<string | null>;
  status: FormControl<string | null>;
}
