import { HttpInterceptorFn } from '@angular/common/http';
import { inject } from '@angular/core';
import { AuthStore } from 'app/store/auth/auth.store';
import { loadingStore } from 'app/store/loading/loading.store';
import { catchError, finalize, throwError } from 'rxjs';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Router } from '@angular/router';

let countRequest = 0;

export const appInterceptor: HttpInterceptorFn = (req, next) => {
  const loadtore = inject(loadingStore);
  const authStore = inject(AuthStore);
  const router: Router = inject(Router);

  const messageService: NzMessageService = inject(NzMessageService);

  let authReq = req.clone();
  if (req.url.indexOf('login') != 1) {
    authReq = req.clone({
      setHeaders: {
        Authorization: `Bearer ${authStore.token()}`,
      },
    });
  }
  if (!countRequest) {
    loadtore.changeLoadingState(true);
  }
  countRequest++;
  return next(authReq).pipe(
    finalize(() => {
      countRequest--;
      if (!countRequest) {
        loadtore.changeLoadingState(false);
      }
    }),
    catchError((err: any) => {
      if (err.status != 401) {
        if (err.error && err.error.message) {
          messageService.error(err.error.message);
        } else {
          messageService.error('Ups! Tenemos problemas con el servidor');
        }
      } else {
        router.navigateByUrl('/login');
      }

      return throwError(() => err);
    })
  );
};
