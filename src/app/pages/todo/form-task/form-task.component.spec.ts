import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTaskComponent } from './form-task.component';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { TaskStore } from 'app/store/task/task.store';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { TypeForm } from './forkTask.general';

describe('FormTaskComponent', () => {
  let component: FormTaskComponent;
  let fixture: ComponentFixture<FormTaskComponent>;

  let taskStore: any;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormTaskComponent, HttpClientModule, NoopAnimationsModule],
      providers: [
        {
          provide: NzModalRef,
          useValue: jasmine.createSpyObj('NzModalRef', [
            'close',
            'destroy',
            'updateConfig',
          ]),
        },
        {
          provide: NZ_MODAL_DATA,
          useValue: {
            type: 'add',
          },
        },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(FormTaskComponent);

    taskStore = TestBed.inject(TaskStore);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('validate edit modal init', () => {
    component.modalData = {
      type: TypeForm.EDIT,
      task: {
        title: 'title',
        description: 'description',
        status: 'por hacer',
      },
    };

    component.ngOnInit();

    expect(component.formTask.valid).toBeTrue();
  });

  it('validate form invalid', () => {
    component.saveForm();
    expect(component.formTask.valid).toBeFalse();
  });

  it('validar form valido con modo agregar', () => {
    component.formTask.patchValue({
      title: 'title',
      description: 'description',
      status: 'por hacer',
    });
    component.modalData = {
      type: TypeForm.ADD,
    };
    const spyAddStore = spyOn(taskStore, 'addTask').and.returnValue({});
    component.saveForm();

    expect(spyAddStore).toHaveBeenCalled();
  });

  it('validar form valido con modo edit', () => {
    component.formTask.patchValue({
      title: 'title',
      description: 'description',
      status: 'por hacer',
    });
    component.modalData = {
      type: TypeForm.EDIT,
      task: {
        id: 1,
        title: 'title',
        description: 'description',
        status: 'por hacer',
      },
    };
    const spyAddStore = spyOn(taskStore, 'updateTask').and.returnValue({});
    component.saveForm();

    expect(spyAddStore).toHaveBeenCalled();
  });
});
