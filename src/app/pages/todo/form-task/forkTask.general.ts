import { Task } from '@models/model/task.model';

export enum TypeForm {
  ADD = 'add',
  EDIT = 'edit',
}

export interface ModalTaskForm {
  type: TypeForm;
  task?: Task;
}
