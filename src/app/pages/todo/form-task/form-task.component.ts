import { Component, OnInit, inject, signal } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { TaskForm } from '@models/forms/task.form';
import { SharedNgZorroModule } from '@modules/shared-ng-zorro/shared-ng-zorro.module';
import { NZ_MODAL_DATA, NzModalRef } from 'ng-zorro-antd/modal';
import { ModalTaskForm, TypeForm } from './forkTask.general';
import { TaskStore } from 'app/store/task/task.store';

@Component({
  selector: 'app-form-task',
  standalone: true,
  imports: [SharedNgZorroModule, ReactiveFormsModule],
  templateUrl: './form-task.component.html',
  styleUrl: './form-task.component.scss',
})
export class FormTaskComponent implements OnInit {
  private readonly modal: NzModalRef = inject(NzModalRef);
  public modalData: ModalTaskForm = inject(NZ_MODAL_DATA);
  private fb: FormBuilder = inject(FormBuilder);
  readonly taskStore = inject(TaskStore);

  status = ['Por hacer', 'En progreso', 'completada'];

  title = signal('');

  formTask: FormGroup = this.fb.group<TaskForm>({
    title: new FormControl(null, [Validators.required]),
    description: new FormControl(null, [Validators.required]),
    status: new FormControl(null, [Validators.required]),
  });

  ngOnInit(): void {
    const titleText =
      this.modalData.type == TypeForm.ADD ? 'Agregar Tarea' : 'Editar Tarea';
    this.title.update(() => titleText);

    if (this.modalData.type == TypeForm.EDIT) {
      this.formTask.patchValue(this.modalData.task!);
    }
  }

  saveForm() {
    if (this.formTask.valid) {
      if (this.modalData.type === TypeForm.ADD) {
        this.taskStore.addTask(this.formTask.value);
      } else {
        this.taskStore.updateTask(
          this.modalData.task!.id!,
          this.formTask.value
        );
      }
      this.modal.close();
    } else {
      Object.values(this.formTask.controls).forEach((control) => {
        if (control.invalid) {
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
    }
  }
}
