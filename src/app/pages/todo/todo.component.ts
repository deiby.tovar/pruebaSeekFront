import { Component, OnInit, inject } from '@angular/core';
import { SharedNgZorroModule } from '@modules/shared-ng-zorro/shared-ng-zorro.module';
import { TaskStore } from 'app/store/task/task.store';
import { NzModalService } from 'ng-zorro-antd/modal';
import { FormTaskComponent } from './form-task/form-task.component';
import { Task } from '@models/model/task.model';

@Component({
  selector: 'app-todo',
  standalone: true,
  imports: [SharedNgZorroModule],
  templateUrl: './todo.component.html',
  styleUrl: './todo.component.scss',
})
export class TodoComponent implements OnInit {
  private readonly modalService: NzModalService = inject(NzModalService);

  readonly taskStore = inject(TaskStore);

  ngOnInit(): void {
    this.taskStore.loadAll();
  }

  addTask() {
    this.modalService.create({
      nzContent: FormTaskComponent,
      nzData: {
        type: 'add',
      },
    });
  }

  editTask(data: Task) {
    this.modalService.create({
      nzContent: FormTaskComponent,
      nzData: {
        type: 'edit',
        task: data,
      },
    });
  }

  deleteTask(id: number) {
    this.taskStore.deleteTask(id);
  }
}
