import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TodoComponent } from './todo.component';
import { TaskStore } from '../../store/task/task.store';
import { HttpClientModule } from '@angular/common/http';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { FormTaskComponent } from './form-task/form-task.component';

describe('TodoComponent', () => {
  let component: TodoComponent;
  let fixture: ComponentFixture<TodoComponent>;

  let taskStore: jasmine.SpyObj<any>;

  beforeEach(async () => {
    const modalSpy = jasmine.createSpyObj('NzModalService', ['create']);
    await TestBed.configureTestingModule({
      imports: [TodoComponent, HttpClientModule, NoopAnimationsModule],
    }).compileComponents();

    taskStore = TestBed.inject(TaskStore);

    fixture = TestBed.createComponent(TodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('deberia abrir modal crear', () => {
    component.addTask();

    expect(component).toBeTruthy();
  });

  it('deberia abrir modal edit', () => {
    component.editTask({
      id: 1,
      title: 'title',
      description: 'description',
      status: 'por hacer',
    });

    expect(component).toBeTruthy();
  });

  it('deberia eliminar task', () => {
    const deleteTask = spyOn(taskStore, 'deleteTask').and.returnValue({});
    component.deleteTask(1);

    expect(deleteTask).toHaveBeenCalled();
  });
});
