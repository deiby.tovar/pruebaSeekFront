import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from '@services/auth.service';
import { SharedNgZorroModule } from '@modules/shared-ng-zorro/shared-ng-zorro.module';
import { UserOutline, LockOutline } from '@ant-design/icons-angular/icons';
import { NZ_ICONS } from 'ng-zorro-antd/icon';
import { of } from 'rxjs';
import { Token } from '@angular/compiler';
import { LoginResponse } from '@models/response/login.response';
import { RouterModule } from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  let authService: AuthService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        LoginComponent,
        HttpClientModule,
        SharedNgZorroModule,
        RouterModule.forRoot([{ path: '**', component: LoginComponent }]),
      ],
      providers: [
        AuthService,
        { provide: NZ_ICONS, useValue: [UserOutline, LockOutline] },
      ],
    }).compileComponents();

    authService = TestBed.inject(AuthService);
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('Validar datos requeridos', () => {
    component.login();
    expect(component.formGroup.valid).toBeFalse();
  });

  it('validar envio formulario', () => {
    component.formGroup.patchValue({
      user: 'text',
      password: 'description',
    });
    const loginSpy = spyOn(authService, 'login').and.returnValue(
      of({
        message: 'login',
        data: {
          token: '123',
        },
      } as LoginResponse)
    );
    component.login();
    expect(loginSpy).toHaveBeenCalled();
  });
});
